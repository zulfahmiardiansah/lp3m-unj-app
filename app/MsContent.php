<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MsContent extends Model
{
    // Table
        protected $table = "ms_content";
    
    // Channel
        public function channel ()
        {
            return $this->belongsTo(MsChannel::class);
        }
}
