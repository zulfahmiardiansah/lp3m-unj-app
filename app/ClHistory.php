<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClHistory extends Model
{
    // Table
        protected $table = "cl_history";

        public function content ()
        {
            return $this->belongsTo(MsContent::class);
        }
}
