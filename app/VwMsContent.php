<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VwMsContent extends Model
{
    // Table
        protected $table = "vw_ms_content";
    
    // Channel
        public function channel ()
        {
            return $this->belongsTo(MsChannel::class);
        }
}
