<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminUserManagementController extends CBController {


    public function cbInit()
    {
        $this->setTable("cl_users");
        $this->setPermalink("user_management");
        $this->setPageTitle("User Management");

        $this->addDatetime("Created At","created_at")->required(false)->showIndex(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("NIDN","nidn")->strLimit(150)->maxLength(255);
		$this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addText("Program","program")->strLimit(150)->maxLength(255);
		$this->addText("Faculty","faculty")->strLimit(150)->maxLength(255);
		$this->addImage("Img Profile","img_profile")->encrypt(true);
		$this->addPassword("Password","password")->required(false)->showIndex(false)->showDetail(false);
		$this->addText("Updated By","updated_by")->required(false)->showAdd(false)->showEdit(false);
		

    }
}
