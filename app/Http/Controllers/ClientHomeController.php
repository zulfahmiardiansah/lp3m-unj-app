<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ClSession;
use App\MsChannel;
use App\MsSetting;

class ClientHomeController extends Controller
{
    public $command;

    public function initiateCommand ()
    {
        $clSession = ClSession::where("session_key", $_COOKIE['4b0a5e8d9b036d2bfd1e608caf79a344'])->first();
        $this->command['session'] = json_decode($clSession->session_value);
        $this->command["channelList"] = MsChannel::orderBy("position", "asc")->get();
    }

    // Home Action
        public function homeAction (Request $request)
        {
            $this->initiateCommand();
            $this->command['homeVideo'] = MsSetting::where("category", "Home Video")->first();
            $this->command['homeInformation'] = MsSetting::where("category", "Home Information")->first();
            return view("home.home", $this->command);
        }
}
