<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminDocumentController extends CBController {


    public function cbInit()
    {
        $this->setTable("ms_doc");
        $this->setPermalink("document");
        $this->setPageTitle("Document");

        $this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addFile("File","file_uri")->showIndex(false)->encrypt(true);
		$this->addFile("Thumbnail","thumbnail")->showIndex(false)->encrypt(true);
		$this->addText("Created By","created_by")->showIndex(false)->showAdd(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addDate("Updated By","updated_by")->showAdd(false)->showEdit(false);
		$this->addSelectOption("Type","type")->options(['Image'=>'Image','Video'=>'Video','Document'=>'Document']);
		

    }
}
