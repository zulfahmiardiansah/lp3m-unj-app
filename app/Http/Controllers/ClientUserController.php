<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ClSession;
use App\ClUser;
use App\ClHistory;
use App\MsChannel;
use App\MsContent;

class ClientUserController extends Controller
{
    public $command;
    public $statusText;
    public $statusType;

    public function initiateCommand ()
    {
        $clSession = ClSession::where("session_key", $_COOKIE['4b0a5e8d9b036d2bfd1e608caf79a344'])->first();
        $this->command['session'] = json_decode($clSession->session_value);
        $this->command["channelList"] = MsChannel::orderBy("name", "asc")->get();
    }

    public function profileAction ()
    {
        $this->initiateCommand();
        return view("user.profile", $this->command);
    }

    public function passwordAction ()
    {
        $this->initiateCommand();
        return view("user.password", $this->command);
    }
    
    public function historyAction ()
    {
        $this->initiateCommand();
        $this->command['historyList'] = ClHistory::where("user_id", $this->command['session']->id)->orderBy('id', 'DESC')->paginate(10);
        return view("user.history", $this->command);
    }
    
    public function profileProcess (Request $request) {
        $this->initiateCommand();
        try {
            $clUserRecord   =   ClUser::find($this->command['session']->id);
            $clUserRecord->name = $request->get('name');
            $clUserRecord->faculty = $request->get('faculty');
            $clUserRecord->program = $request->get('program');
            $file = $request->file("img_profile");
            if ($file != false) {
                $storageUrl =   "storage/files/" . date("Y") . "/" . date("m") . "/" . date("d") . "/";
                $newProfileName =  rand(00000000,9999999) . "_" . $file->getClientOriginalName();
                $file->move($storageUrl, $newProfileName);
                $clUserRecord->img_profile =  $storageUrl . $newProfileName;
            }
            $clUserRecord->save();
            ClSession::where("session_key", $_COOKIE['4b0a5e8d9b036d2bfd1e608caf79a344'])->delete();
            $clSession = new ClSession();
            $clSession->session_key = $_COOKIE['4b0a5e8d9b036d2bfd1e608caf79a344'];
            $clSession->session_value = json_encode($clUserRecord);
            $clSession->save();
            $this->statusType = "success";
            $this->statusText = "Pengubahan data diri berhasil.";
        } catch (\Throwable $th) {
            $this->statusType = "error";
            $this->statusText = $th->getMessage();
        }
        return \Redirect::to(route('profile'))->with($this->statusType, $this->statusText);
    }

    public function passwordProcess(Request $request)
    {
        $this->initiateCommand();
        try {
            $clUserRecord   =   ClUser::find($this->command['session']->id);
            if ($request->get('newPassword') == $request->get('confirmNewPassword')) {
                if (\Hash::check($request->get("oldPassword"), $clUserRecord->password)) {
                    $clUserRecord->password = \Hash::make($request->get('newPassword'));
                    $clUserRecord->save();
                    $this->statusType = "success";
                    $this->statusText = "Ubah kata sandi berhasil.";
                    return \Redirect::to(route('home'))->with($this->statusType, $this->statusText);
                } else {
                    throw new \Exception("Kata sandi lama anda tidak valid.", 1);
                }
            } else {
                throw new \Exception("Form yang anda isi tidak tepat.", 1);
            }
        } catch (\Throwable $th) {
            $this->statusType = "error";
            $this->statusText = $th->getMessage();
        }
        return \Redirect::to(route('password'))->with($this->statusType, $this->statusText);
    }
}
