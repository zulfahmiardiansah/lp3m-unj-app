<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminAdministratorController extends CBController {


    public function cbInit()
    {
        $this->setTable("users");
        $this->setPermalink("administrator");
        $this->setPageTitle("Administrator");

        $this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addEmail("Email","email");
		$this->addDatetime("Email Verified At","email_verified_at")->showAdd(false)->showEdit(false);
		$this->addPassword("Password","password");
		$this->addText("Remember Token","remember_token")->required(false)->showIndex(false)->showDetail(false)->showAdd(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false)->showIndex(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addImage("Photo","photo")->encrypt(true);
		$this->addSelectTable("Role","cb_roles_id",["table"=>"cb_roles","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addText("Ip Address","ip_address")->required(false)->showIndex(false)->showAdd(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addText("User Agent","user_agent")->required(false)->showIndex(false)->showAdd(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addDatetime("Login At","login_at")->required(false)->showIndex(false)->showAdd(false)->showEdit(false);
		

    }
}
