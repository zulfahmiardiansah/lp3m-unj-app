<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminLogActivityController extends CBController {


    public function cbInit()
    {
        $this->setTable("lg_activity");
        $this->setPermalink("log_activity");
        $this->setPageTitle("Log Activity");

        $this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showIndex(false)->showDetail(false)->showAdd(false)->showEdit(false);
		$this->addText("Log","text")->strLimit(150)->maxLength(255);
		

    }
}
