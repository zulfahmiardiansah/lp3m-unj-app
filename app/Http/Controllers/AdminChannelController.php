<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminChannelController extends CBController {


    public function cbInit()
    {
        $this->setTable("ms_channel");
        $this->setPermalink("channel");
        $this->setPageTitle("Channel");

        $this->addDatetime("Created At","created_at")->required(false)->showIndex(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addText("Short Desc","short_desc")->strLimit(150)->maxLength(255);
		$this->addTextArea("Long Desc","long_desc")->showIndex(false)->strLimit(150);
		$this->addImage("Avatar","avatar")->encrypt(true);
		$this->addText("Created By","created_by")->required(false)->showIndex(false)->showAdd(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addText("Updated By","updated_by")->required(false)->showAdd(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addNumber("Position","position");
		
		$this->addSubModule("Content", AdminContentController::class, "channel_id", function ($row) {
			return [
				"Name" => $row->name,
				"Short Description" => $row->short_desc,
				"Long Description" => $row->long_desc,
			];
		});
    }
}
