<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminSettingController extends CBController {


    public function cbInit()
    {
        $this->setTable("ms_setting");
        $this->setPermalink("setting");
        $this->setPageTitle("Setting");

        $this->addDatetime("Created At","created_at")->required(false)->showIndex(false)->showDetail(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showIndex(false)->showDetail(false)->showAdd(false)->showEdit(false);
		$this->addSelectOption("Category","category")->options(['Home Video'=>'Home Video','Login Video'=>'Login Video','Home Information'=>'Home Information']);
		$this->addText("Media URL","video_url")->required(false)->strLimit(150)->maxLength(255);
		$this->addFile("Media File","media_url")->required(false)->encrypt(true);
		

    }
}
