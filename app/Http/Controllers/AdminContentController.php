<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminContentController extends CBController {


    public function cbInit()
    {
        $this->setTable("ms_content");
        $this->setPermalink("content");
        $this->setPageTitle("Content");

        $this->addDatetime("Created At","created_at")->required(false)->showIndex(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addText("Short Desc","short_desc")->strLimit(150)->maxLength(255);
		$this->addTextArea("Long Desc","long_desc")->showIndex(false)->strLimit(150);
		$this->addImage("Thumbnail","thumbnail")->encrypt(true);
		$this->addText("Video Url","video_url")->showIndex(false)->strLimit(150)->maxLength(255);
		$this->addSelectTable("Channel","channel_id",["table"=>"ms_channel","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addText("Created By","created_by")->required(false)->showIndex(false)->showAdd(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addDate("Updated By","updated_by")->required(false)->showAdd(false)->showEdit(false);
		$this->addNumber("Position","position");
		$this->addNumber("Total View","view")->required(false)->showAdd(false)->showEdit(false);
		
		/* First parameter is sub module name
		* Second parameter is sub module Controller (You have to create it first)
		* Third parameter is foreign key from cities table
		* Fourth parameter is for detail attributes */
		$this->addSubModule("Content", AdminContentController::class, "channel_id", function ($row) {
			return [
				"Name" => $row->name,
				"Short Description" => $row->short_desc,
				"Long Description" => $row->long_desc,
			];
		});
    }
}
