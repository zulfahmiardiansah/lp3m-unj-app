<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ClSession;
use App\ClHistory;
use App\MsChannel;
use App\MsContent;
use App\MsDoc;

class ClientDocController extends Controller
{
    public $command;
    public $statusText;
    public $statusType;

    public function initiateCommand ()
    {
        $clSession = ClSession::where("session_key", $_COOKIE['4b0a5e8d9b036d2bfd1e608caf79a344'])->first();
        $this->command['session'] = json_decode($clSession->session_value);
        $this->command["channelList"] = MsChannel::orderBy("name", "asc")->get();
    }

    public function docAction ()
    {
        $this->initiateCommand();
        $this->command['docList'] = MsDoc::orderBy("id", "DESC")->paginate(10);
        return view("doc.list", $this->command);
    }

}
