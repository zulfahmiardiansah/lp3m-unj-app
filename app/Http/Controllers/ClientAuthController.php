<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ClUser;
use App\ClSession;
use App\MsSetting;

class ClientAuthController extends Controller
{
    // Property
        public $statusText;
        public $statusType;
        public $command;

    // Login Action
        public function loginAction ()
        {
            if (isset($_COOKIE['4b0a5e8d9b036d2bfd1e608caf79a344'])) {
                $clSessionRec = ClSession::where("session_key", $_COOKIE['4b0a5e8d9b036d2bfd1e608caf79a344'])
                                    ->first();
                if ($clSessionRec) {
                    return \Redirect::to(route('home'));
                }
            }
            $this->command['loginVideo'] = MsSetting::where("category", "Home Video")->first();
            return view("auth.login", $this->command);
        }

    // Login Proccess
        public function loginProcess (Request $request)
        {
            try {
                $isRequestNotValid  =   \Validator::make($request->all(), [
                                            "nidn" => "required",
                                            "password" => "required"
                                        ])->fails();
                if ($isRequestNotValid) {
                    throw new \Exception("Informasi yang diberikan tidak lengkap.", 1);
                } else {
                    $clUserModel    =   ClUser::where("nidn", $request->get("nidn"))->first();
                    if ($clUserModel) {
                        if (\Hash::check($request->get("password"), $clUserModel->password)) {
                            $sessionKey = bcrypt(rand(000000000000, 999999999999));
                            $clSession = new ClSession();
                            $clSession->session_key = $sessionKey;
                            $clSession->session_value = json_encode($clUserModel);
                            $clSession->save();
                            if ($request->get("remember")) {
                                setcookie("4b0a5e8d9b036d2bfd1e608caf79a344", $sessionKey, time()+86400*30);
                            } else {
                                setcookie("4b0a5e8d9b036d2bfd1e608caf79a344", $sessionKey);
                            }
                            return redirect(route("home"));
                        }
                    }
                }
                throw new \Exception("Akun anda tidak terdaftar.", 1);
            } catch (\Throwable $th) {
                $this->statusType = "error";
                $this->statusText = $th->getMessage();
            }
            return redirect(route("login"))->with($this->statusType, $this->statusText);
        }

    // Logout Process
        public function logoutProcess ()
        {
            $clSessionRec = ClSession::where("session_key", $_COOKIE['4b0a5e8d9b036d2bfd1e608caf79a344']);
            $clSessionRec->delete();
            setcookie("4b0a5e8d9b036d2bfd1e608caf79a344", false, -1);
            $this->statusType = "success";
            $this->statusText = "Logout berhasil.";
            return redirect(route("login"))->with($this->statusType, $this->statusText);
        }

}
