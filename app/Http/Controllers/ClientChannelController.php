<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ClSession;
use App\ClHistory;
use App\MsChannel;
use App\MsContent;
use App\VwMsContent;

class ClientChannelController extends Controller
{
    public $command;
    public $statusText;
    public $statusType;

    public function initiateCommand ()
    {
        $clSession = ClSession::where("session_key", $_COOKIE['4b0a5e8d9b036d2bfd1e608caf79a344'])->first();
        $this->command['session'] = json_decode($clSession->session_value);
        $this->command["channelList"] = MsChannel::orderBy("name", "asc")->get();
    }

    public function searchAction (Request $request) {
        $this->initiateCommand();
        $this->command['contentList'] = MsContent::where("name",  "LIKE" , '%' . $request->get('keyword') . '%')
                                            ->orderBy("updated_at", 'DESC')
                                            ->paginate("10")
                                            ->withPath("?keyword=" . $request->get("keyword"));
        $this->command['keyword'] = $request->get('keyword');
        return view("video.search", $this->command);
    }

    public function trendingAction ()
    {
        $this->initiateCommand();
        $this->command['contentList'] = MsContent::orderBy("view", "DESC")->limit(10)->get();
        return view("video.trending", $this->command);
    }

    public function singleAction (Request $request, string $channel_slug, int $content_position)
    {
        $this->initiateCommand();
        try {
            $this->command['channel'] = MsChannel::where("slug_id", $channel_slug)->first();
            $this->command['content'] = MsContent::where("channel_id", $this->command['channel']->id)
                                                    ->where("position", $content_position)
                                                    ->first();
            $this->command['nextContent'] = MsContent::where("channel_id", $this->command['channel']->id)
                                                    ->where("position", $content_position + 1)
                                                    ->first();
            $this->command['content']->view     =   $this->command['content']->view + 1;
            $this->command['content']->save();
            $this->command['contentList'] = MsContent::where("channel_id", $this->command['channel']->id)
                                                ->orderBy("position", 'asc')->get();
            $this->saveHistoryAction($this->command['content']->id, $this->command['session']->id);
            return view("video.single", $this->command);
        } catch (\Throwable $th) {
            $this->statusType = "error";
            $this->statusText = "Terjadi Kesalahan !";
            return \Redirect::to(route('home'))->with($this->statusType, $this->statusText);
        }
    }

    public function allAction (Request $request, int $content_position)
    {
        $this->initiateCommand();
        try {
            $this->command['contentList'] = VwMsContent::all();
            $countContent = count($this->command['contentList']);
            $nextContent = $content_position + 1;
            $this->command['content'] = $this->command['contentList'][$content_position - 1];
            $this->command['content']->view     =   $this->command['content']->view + 1;
            $this->command['content']->save();
            if ($nextContent <= $countContent) {
                $this->command['nextContent'] = $nextContent;
            } else {
                $this->command['nextContent'] = null;
            }
            $this->command['contentPosition'] = $content_position;
            $this->saveHistoryAction($this->command['content']->id, $this->command['session']->id);
            return view("video.all", $this->command);
        } catch (\Throwable $th) {
            $this->statusType = "error";
            $this->statusText = "Terjadi Kesalahan !";
            return \Redirect::to(route('home'))->with($this->statusType, $this->statusText);
        }
    }

    public function saveHistoryAction (int $content_id, int $user_id)
    { 
        $this->initiateCommand();
        $listClHistory = ClHistory::where("user_id", $user_id)->orderBy('id', 'DESC')->get();
        if (count($listClHistory) > 0) {
            if ($listClHistory[0]->content_id == $content_id) {
                return true;
            } else {
                if (count($listClHistory) == 20) {
                    ClHistory::find($listClHistory[19]->id)->delete();
                }
            }
        }
        $clHistory = new ClHistory();
        $clHistory->content_id = $content_id;
        $clHistory->user_id = $user_id;
        $clHistory->save();
        return true;
    }
}
