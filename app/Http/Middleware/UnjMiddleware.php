<?php

namespace App\Http\Middleware;

use Closure;
use App\ClSession;

class UnjMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($_COOKIE['4b0a5e8d9b036d2bfd1e608caf79a344'])) {
            $clSessionRec = ClSession::where("session_key", $_COOKIE['4b0a5e8d9b036d2bfd1e608caf79a344'])
                                ->first();
            if ($clSessionRec) {
                return $next($request);
            }
        }
        return redirect(route("login"))->with("error", "Anda harus login terlebih dahulu.");
    }
}
