<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get("", "ClientAuthController@loginAction")
    ->name("login");
Route::post("", "ClientAuthController@loginProcess");

Route::middleware(["web", "lppmUnj"])->group(function () {
    
    Route::get("logout", "ClientAuthController@logoutProcess")
        ->name("logout");

    Route::get("home", "ClientHomeController@homeAction")
        ->name("home");
        
    Route::get("history", "ClientUserController@historyAction")
        ->name("history");

    Route::get("profile", "ClientUserController@profileAction")
        ->name("profile");

    Route::get("password", "ClientUserController@passwordAction")
        ->name("password");

    Route::get("doc", "ClientDocController@docAction")
        ->name("doc");

    Route::post("profile", "ClientUserController@profileProcess");
    Route::post("password", "ClientUserController@passwordProcess");
        
    Route::get("search", "ClientChannelController@searchAction")
        ->name("search");
    Route::get("trending", "ClientChannelController@trendingAction")
        ->name("trending");
        
    Route::get("channel/{channel_slug}/{content_position}", "ClientChannelController@singleAction")
        ->name("channel.singleVideo");

    Route::get("all-channel/{content_position}", "ClientChannelController@allAction")
        ->name("channel.allVideo");
});