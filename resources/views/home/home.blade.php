@include("includes.header")

<div class="row mb-3" id="welcome-section">
    <div class="col-md-12 mb-3">
        <div class="mr-md-3 mr-xl-5">
            <h3>Selamat Datang</h3>
        </div>
    </div>
    <div class="col-md-5 mb-2 px-4 py-2">
        <?php $ext = explode(".", $homeVideo->video_url); ?>
        <video id="my-video" class="video-js" controls preload="auto" data-setup="{}">
            <source src="{{ $homeVideo->video_url }}" type="video/{{ end($ext) }}" />
            <p class="vjs-no-js">
                To view this video please enable JavaScript, and consider upgrading to a
                web browser that
                <a href="https://videojs.com/html5-video-support/" target="_blank"
                    >supports HTML5 video</a>
            </p>
        </video>
    </div>
    <div class="col-md-7 mb-2 px-4 py-2">
        <img src="{{ asset($homeInformation->media_url) }}" alt="">
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-3">
        <div class="mr-md-3 mr-xl-5">
            <h3>Rekomendasi</h3>
        </div>
    </div>
    @foreach ($channelList as $channel)
        <div class="col-md-4 mb-2 px-4 py-2 channel-link">
            <a href="{{ route('channel.singleVideo', ['channel_slug' => $channel->slug_id, 'content_position' => 1]) }}">
                <img src="{{ $channel->avatar }}" alt="">
                <h4 class="mt-3">
                    {{ $channel->name }}
                </h4>
                <p class="text-justify text-muted">
                    {{ $channel->short_desc }}
                </p>
            </a>
        </div>
    @endforeach
</div>

@include("includes.footer")

<script>
    $('li[data-uri="Beranda"]').addClass("active");
</script>