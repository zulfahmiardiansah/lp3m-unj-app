@include("includes.header")

<style>
    
    .profile-form .edit-mode {
        display: none;
    }
</style>

<div class="row mb-3">
    <div class="col-md-12">
        <h3 class="mb-4 pb-1">
            History
        </h3>
        <div class="row channel-link">
            @if (count($historyList) == 0) 
                <div class="empty-container">
                    <img src="{{ asset('cl_asset/images/empty.svg') }}" class="empty-img">
                    <p class="text-muted">
                        Data tidak ditemukan
                    </p>
                </div>
            @endif
            @foreach ($historyList as $history)
                <div class="col-md-3 mb-4">
                    <a href="{{ route('channel.singleVideo', ['channel_slug' => $history->content->channel->slug_id, 'content_position' => $history->content->position]) }}">
                        <img src="{{ asset($history->content->thumbnail) }}" alt="">
                    </a>
                </div>
                <div class="col-md-8 mb-4 px-3">
                    <a href="{{ route('channel.singleVideo', ['channel_slug' => $history->content->channel->slug_id, 'content_position' => $history->content->position]) }}">
                        <h4>
                            {{ $history->content->name }}
                        </h4>
                        <h5 class="mb-2 mt-2 py-1">
                            <i class="mdi mdi-play-protected-content video-icon"></i><span class="video-meta">{{ $history->content->channel->name }}</span>
                            <i class="mdi mdi-eye video-icon"></i><span class="video-meta">{{ $history->content->view + 0 }} kali dilihat</span>
                            <i class="mdi mdi mdi-calendar  video-icon"></i><span class="video-meta">Dilihat pada {{ date("H.i, d M Y", strtotime($history->created_at)) }}</span>
                        </h5>
                        <p class="text-justify text-muted">
                            {{ $history->content->short_desc }}
                        </p>
                    </a>
                </div>
            @endforeach
        </div>
        {{ $historyList->links() }}
    </div>
</div>

@include("includes.footer")

<script>
    $('li[data-uri="History"]').addClass("active");
</script>
