@include("includes.header")

<form class="forms-sample profile-form" action="" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row mb-3">
        <div class="col-md-12">
            <div class="mr-md-3 mr-xl-5 mb-4">
                <h3>Ubah Kata Sandi</h3>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="form-group mb-2">
                        <label>Kata Sandi Lama</label>
                        <input name="oldPassword" placeholder="Kata Sandi Lama" type="password" required="" class="form-control">
                    </div>
                    <div class="form-group mb-2">
                        <label>Kata Sandi Baru</label>
                        <input name="newPassword" placeholder="Kata Sandi Baru" type="password" required="" class="form-control">
                    </div>
                    <div class="form-group mb-3">
                        <label>Konfirmasi Kata Sandi Baru</label>
                        <input name="confirmNewPassword" placeholder="Kata Sandi" type="password" required="" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-sm btn-success mr-2">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</form>


@include("includes.footer")
