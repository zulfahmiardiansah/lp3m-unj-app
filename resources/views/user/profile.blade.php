@include("includes.header")

<style>
    
    .profile-form .edit-mode {
        display: none;
    }
</style>

<form class="forms-sample profile-form" action="" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row mb-3">
        <div class="col-md-12">
            <div class="mr-md-3 mr-xl-5 mb-4">
                <h3>Informasi Akun</h3>
            </div>
        </div>
        <div class="col-md-4 mb-sm-4">
            <div class="card">
                <div class="card-body">
                    <img src="{{ asset($session->img_profile) }}" alt="profile" class="img-thumbnail my-1 p-3" />
                    <div class="form-group mt-2 edit-mode" style="overflow: hidden">
                        <label for="exampleInputUsername1">Ubah Foto Profil</label>
                        <input type="file" name="img_profile" style="transform: scale(.9) translateX(-10px)">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                        <div class="form-group mb-2">
                            <label for="exampleInputUsername1">NIDN</label>
                            <p class="px-2 py-2">
                                {{ $session->nidn }}
                            </p>
                        </div>
                        <div class="form-group mb-2">
                            <label for="exampleInputUsername1">Nama</label>
                            <p class="px-2 py-2 view-mode">
                                {{ $session->name }}
                            </p>
                            <input type="text" required="" class="form-control edit-mode" placeholder="Nama" name="name" value="{{ $session->name }}">
                        </div>
                        <div class="form-group mb-2">
                            <label for="exampleInputEmail1">Fakultas</label>
                            <p class="px-2 py-2 view-mode">
                                {{ $session->faculty }}
                            </p>
                            <input type="text" required="" class="form-control edit-mode" placeholder="Fakultas" name="faculty" value="{{ $session->faculty }}">
                        </div>
                        <div class="form-group mb-2">
                            <label for="exampleInputPassword1">Program Studi</label>
                            <p class="px-2 py-2 view-mode">
                                {{ $session->program }}
                            </p>
                            <input type="text" required="" class="form-control edit-mode" placeholder="Program Studi" name="program" value="{{ $session->program }}">
                        </div>
                        <div class="row mt-3">
                            <div class="col text-left">
                                <button type="button" class="btn btn-sm btn-danger mr-2 edit-mode" onclick="changeMode('view')">Batalkan</button>
                            </div>
                            <div class="col text-right">
                                <button type="submit" class="btn btn-sm btn-success mr-2 edit-mode">Simpan</button>
                                <button type="button" class="btn btn-sm btn-primary mr-2 view-mode" onclick="changeMode('edit')">Ubah</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</form>


@include("includes.footer")

<script>
    $('li[data-uri="Home"]').addClass("active");
    mode = "view";
    function changeMode (e) {
        $("." + e + "-mode").show();
        $(".profile-form")[0].reset();
        $("." + mode + "-mode").hide();
        mode = e;
    }
</script>
