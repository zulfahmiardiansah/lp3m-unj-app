@include("includes.header")

<style>
    .pdf-card {
        width: 220px;
        display: inline-block;
    }

    .pdf-container {
        display: flex;
        width: 100%;
    }

    .pdf-card {
        margin: 10px 15px;
        background-color: white;
        border-radius: 7px;
        overflow: hidden;
        box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.03);
    }

    .pdf-content {
        padding: 15px 20px;
    }

    .pdf-content h5 {
        line-height: 1.5em;
        text-align: center;
        font-size: .85em;
    }

    .pdf-container {
        display: flex;
        flex-wrap: wrap;
        align-content: center;
    }

    @media (max-width: 768px) {
        .pdf-container {
            justify-content: center;
        }
    }

    span.video-meta {
        font-size: .8em;
    }

    .pdf-content i.video-icon {
        font-size: .8em;
    }

    .pdf-content {
        text-align: center;
    }

    .pdf-card a {
        color: #222;
        transition: .3s;
    }

    .pdf-card a:hover {
        text-decoration: none;
    }

    .pdf-card a:hover h5 {
        color: #00923f;
    }

</style>

<div class="row mb-1">
    <div class="col-md-12">
        <h3 class="mb-4 pb-1">
            Dokumen
        </h3>
    </div>
    <div class="pdf-container">
        @foreach ($docList as $doc)
        <div class="pdf-card">
            <a href="{{ asset($doc->file_uri) }}" target="_blank">
                <div class="pdf-thumbnail">
                    <!-- <img data-pdf-thumbnail-file="{{ asset($doc->file_uri) }}"> -->
                    <img src="{{ asset($doc->thumbnail) }}" alt=" ">
                </div>
                <div class="pdf-content">
                    <h5>
                        {{ $doc->name }}
                    </h5>
                    <i class="mdi mdi mdi-calendar  video-icon"></i><span class="video-meta">Diunggah
                        {{ date("d M Y", strtotime($doc->created_at)) }}</span>
                </div>
            </a>
        </div>
        @endforeach
    </div>
    <div class="col-md-12 mt-4">
        {{ $docList->links() }}
    </div>
</div>

@include("includes.footer")

<script>
    $('li[data-uri="Dokumen"]').addClass("active");

</script>
