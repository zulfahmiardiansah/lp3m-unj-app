@include("includes.header")

<div class="row mb-3 single-video">
    <div class="col-md-12">
        <h3 class="mb-4 pb-1">
            Hasil pencarian untuk `{{ $keyword }}`
        </h3>
        <div class="row channel-link">
            @if (count($contentList) == 0) 
                <div class="empty-container">
                    <img src="{{ asset('cl_asset/images/empty.svg') }}" class="empty-img">
                    <p class="text-muted">
                        Data tidak ditemukan
                    </p>
                </div>
            @endif
            @foreach ($contentList as $content)
                <div class="col-md-3 mb-4">
                    <a href="{{ route('channel.singleVideo', ['channel_slug' => $content->channel->slug_id, 'content_position' => $content->position]) }}">
                        <img src="{{ asset($content->thumbnail) }}" alt="">
                    </a>
                </div>
                <div class="col-md-8 mb-4 px-3">
                    <a href="{{ route('channel.singleVideo', ['channel_slug' => $content->channel->slug_id, 'content_position' => $content->position]) }}">
                        <h4>
                            {{ $content->name }}
                        </h4>
                        <h5 class="mb-2 mt-2 py-1">
                            <i class="mdi mdi-play-protected-content video-icon"></i><span class="video-meta">{{ $content->channel->name }}</span>
                            <i class="mdi mdi-eye video-icon"></i><span class="video-meta">{{ $content->view + 0 }} kali dilihat</span>
                        </h5>
                        <p class="text-justify text-muted">
                            {{ $content->short_desc }}
                        </p>
                    </a>
                </div>
            @endforeach
        </div>
        {{ $contentList->links() }}
    </div>
</div>


@include("includes.footer")

<script>
    $('li[data-uri="Channel"]').addClass("active");
</script>
