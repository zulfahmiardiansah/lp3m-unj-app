@include("includes.header", ['bodyType' => "sidebar-icon-only"])

<div class="row mb-3 single-video">
    <div class="col-md-8 mb-2 px-4 py-2">
        <?php  $ext = explode(".", $content->video_url); ?>
        <video id="singleVideo" class="video-js" controls preload="auto">
            <source src="{{ $content->video_url }}" type="video/{{ end($ext) }}" />
            <p class="vjs-no-js">
                To view this video please enable JavaScript, and consider upgrading to a
                web browser that
                <a href="https://videojs.com/html5-video-support/" target="_blank"
                    >supports HTML5 video</a>
            </p>
        </video>
        <h1 class="mt-4 mb-3">
            {{ $content->name }}
        </h1>
        <h5 class="mb-3">
            <i class="mdi mdi-play-protected-content video-icon"></i><span class="video-meta">{{ $channel->name }}</span>
            <i class="mdi mdi-eye video-icon"></i><span class="video-meta">{{ $content->view + 0 }} kali dilihat</span>
        </h5>
        <p class="text-justify text-muted">
            {{ $content->long_desc }}
        </p>
    </div>
    <div class="col-md-4 mb-2 px-4 py-2">
        <div class="row">
            <div class="col-md-12">
                <h3 class="mb-3">
                    {{ $channel->name }}
                </h3>
            </div>
            @foreach ($contentList as $content2)
                <div class="col-md-6 mb-2 px-3 py-2 channel-link">
                    <a href="{{ route('channel.singleVideo', ['channel_slug' => $channel->slug_id, 'content_position' => $content2->position]) }}">
                        <img src="{{ asset($content2->thumbnail) }}" alt="">
                        <p class="mt-3">
                            @if ($content2->position == $content->position)
                            <b>{{ $content2->name }}</b>
                            @else
                            {{ $content2->name }}
                            @endif
                        </p>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>


@include("includes.footer")
<script>
    $('li[data-uri="Channel"]').addClass("active");
</script>

<script>
    var player = videojs('singleVideo', {}, function onPlayerReady() {
        videojs.log('Your player is ready!');
        this.play();
        this.on('ended', function() {
            @if ($nextContent)
                window.location = "{{ route('channel.singleVideo', ['channel_slug' => $channel->slug_id, 'content_position' => $nextContent->position]) }}";
            @endif
        });
    });
</script>