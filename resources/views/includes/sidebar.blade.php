<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item" data-uri="Beranda">
            <a class="nav-link" href="{{ route('home') }}">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Beranda</span>
            </a>
        </li>
        <li class="nav-item" data-uri="Trending">
            <a class="nav-link" href="{{ route('trending') }}">
                <i class="mdi mdi-sort menu-icon"></i>
                <span class="menu-title">Trending Video</span>
            </a>
        </li>
        <li class="nav-item" data-uri="History">
            <a class="nav-link" href="{{ route('history') }}">
                <i class="mdi mdi mdi-history menu-icon"></i>
                <span class="menu-title">Histori</span>
            </a>
        </li>
        <li class="nav-item" data-uri="Channel">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class=" mdi mdi mdi-play-protected-content menu-icon"></i>
                <span class="menu-title">Channel</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse show" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    @foreach ($channelList as $channel)
                        <li class="nav-item" data-uri="Channel-{{ $channel->id }}">
                            <a class="nav-link" href="{{ route('channel.singleVideo', ['channel_slug' => $channel->slug_id, 'content_position' => 1]) }}">
                                {{ $channel->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </li>
        <li class="nav-item" data-uri="All Channel">
            <a class="nav-link" href="{{ route('channel.allVideo', ['content_position' => 1]) }}">
                <i class="mdi mdi-bookmark-outline menu-icon"></i>
                <span class="menu-title">Semua Video</span>
            </a>
        </li>
        <li class="nav-item" data-uri="Dokumen">
            <a class="nav-link" href="{{ route('doc') }}">
                <i class=" mdi mdi-file-tree  menu-icon"></i>
                <span class="menu-title">Dokumen</span>
            </a>
        </li>
    </ul>
</nav>
