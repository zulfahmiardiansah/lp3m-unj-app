
                </div>
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 <a
                                href="" target="_blank">LPPM UNJ</a>. All rights reserved.</span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made
                            with <i class="mdi mdi-heart text-danger"></i></span>
                    </div>
                </footer>
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <script src="{{ asset('cl_asset/vendors/base/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('cl_asset/vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('cl_asset/vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('cl_asset/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('cl_asset/js/off-canvas.js') }}"></script>
    <script src="{{ asset('cl_asset/js/toastr.min.js') }}"></script>
    <script src="{{ asset('cl_asset/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('cl_asset/js/template.js') }}"></script>
    <script src="{{ asset('cl_asset/js/dashboard.js') }}"></script>
    <script src="{{ asset('cl_asset/js/data-table.js') }}"></script>
    <script src="{{ asset('cl_asset/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('cl_asset/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('cl_asset/js/pdf.js') }}"></script>
    <script src="{{ asset('cl_asset/js/pdfThumbnails.js') }}"></script>
    <script src="https://vjs.zencdn.net/7.5.5/video.js"></script>
    
    @if(Session::has('error'))
        <script>
            toastr["error"]("{{ Session::get('error') }}")
        </script>
    @endif
    @if(Session::has('success'))
        <script>
            toastr["success"]("{{ Session::get('success') }}")
        </script>
    @endif
</body>

</html>
