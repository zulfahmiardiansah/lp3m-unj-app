<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>LPPM UNJ</title>
    <link rel="stylesheet" href="{{ asset('cl_asset/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('cl_asset/vendors/base/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('cl_asset/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('cl_asset/css/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('cl_asset/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('cl_asset/css/custom.css') }}">
    <link rel="shortcut icon" href="{{ asset('cl_asset/images/logo-mini.png') }}" />
    <link href="https://vjs.zencdn.net/7.5.5/video-js.css" rel="stylesheet" />
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    <style>
        .video-js .vjs-big-play-button {
            position: relative;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%) scale(1.4);
        }
    </style>
</head>

<body class="{{ $bodyType ?? '' }}">
    <div class="container-scroller">
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="navbar-brand-wrapper d-flex justify-content-center">
                <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">
                    <a class="navbar-brand brand-logo" href="{{ route('home') }}"><img src="{{ asset('cl_asset/images/logo.png') }}" alt="logo" /></a>
                    <a class="navbar-brand brand-logo-mini" href="{{ route('home') }}"><img src="{{ asset('cl_asset/images/logo-mini.png') }}"
                            alt="logo" /></a>
                    <button class="navbar-toggler navbar-toggler align-self-center" type="button"
                        data-toggle="minimize">
                        <span class="mdi mdi-sort-variant"></span>
                    </button>
                </div>
            </div>

            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <ul class="navbar-nav mr-lg-4 w-100">
                    <li class="nav-item nav-search d-none d-lg-block w-100">
                        <form action="{{ route('search') }}" method="GET">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="search">
                                    <i class="mdi mdi-magnify"></i>
                                </span>
                            </div>
                            <input type="text" name="keyword" value="{{ isset($keyword) ? $keyword : '' }}" class="form-control" placeholder="Ingin mencari sesuatu ?"
                                aria-label="search" aria-describedby="search">
                        </div>
                        </form>
                    </li>
                </ul>
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <img src="{{ asset($session->img_profile) }}" alt="profile" />
                            <span class="nav-profile-name">{{ $session->name }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown"
                            aria-labelledby="profileDropdown">
                            <a class="dropdown-item" href="{{ route('profile') }}">
                                <i class="mdi mdi-settings text-primary"></i>
                                Informasi Akun
                            </a>
                            <a class="dropdown-item" href="{{ route('password') }}">
                                <i class=" mdi mdi-key  text-primary"></i>
                                Ubah Kata Sandi
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}">
                                <i class="mdi mdi-logout text-primary"></i>
                                Keluar
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="ml-3 navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_sidebar.html -->
            @include("includes.sidebar")
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">