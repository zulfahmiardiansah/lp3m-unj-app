<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>LPPM UNJ</title>
    <link rel="stylesheet" href="cl_asset/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="cl_asset/vendors/base/vendor.bundle.base.css">
    <link rel="stylesheet" href="cl_asset/css/style.css">
    <link rel="shortcut icon" href="cl_asset/images/logo-mini.png" />
    <link rel="stylesheet" href="cl_asset/css/toastr.min.css">
    <link rel="stylesheet" href="cl_asset/css/custom_login.css">
    <link href="https://vjs.zencdn.net/7.5.5/video-js.css" rel="stylesheet" />
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    <style>
        .video-js .vjs-big-play-button {
            position: relative;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%) scale(1.4);
        }
    </style>
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth px-0">
                <div class="row w-100 mx-0 px-3">
                    <div class="col-lg-8 p-2 fHome">
                        <?php $ext = explode(".", $loginVideo->video_url); ?>
                        <video id="my-video" class="video-js" controls preload="auto" data-setup="{}">
                            <source src="{{ $loginVideo->video_url }}" type="video/{{ end($ext) }}" />
                            <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a
                                web browser that
                                <a href="https://videojs.com/html5-video-support/" target="_blank"
                                    >supports HTML5 video</a>
                            </p>
                        </video>
                    </div>
                    <div class="col-lg-4 p-2">
                        <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                            <div class="brand-logo">
                                <img src="cl_asset/images/logo.png" alt="logo">
                            </div>
                            <h3>Hai ! Mari kita mulai,</h3>
                            <h6 class="font-weight-light">Login untuk melanjutkan.</h6>
                            <form class="pt-3" action="" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="text" name="nidn" class="form-control form-control-lg" placeholder="NIDN" required="">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control form-control-lg" placeholder="Kata Sandi" required="">
                                </div>
                                <div class="mt-3">
                                    <button class="btn btn-block btn-success btn-lg font-weight-medium auth-form-btn"
                                        >MASUK</button>
                                </div>
                                <div class="my-2 d-flex justify-content-between align-items-center">
                                    <div class="form-check">
                                        <label class="form-check-label text-muted">
                                            <input type="checkbox" name="remember" value="y" class="form-check-input">
                                            Ingat saya
                                        </label>
                                    </div>
                                    <!-- <a href="#" class="auth-link text-black">Lupa kata sandi ?</a> -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="cl_asset/vendors/base/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="cl_asset/js/off-canvas.js"></script>
    <script src="cl_asset/js/hoverable-collapse.js"></script>
    <script src="cl_asset/js/toastr.min.js"></script>
    <script src="cl_asset/js/template.js"></script>

    <script src="https://vjs.zencdn.net/7.5.5/video.js"></script>

    @if(Session::has('error'))
        <script>
            toastr["error"]("{{ Session::get('error') }}")
        </script>
    @endif
    @if(Session::has('success'))
        <script>
            toastr["success"]("{{ Session::get('success') }}")
        </script>
    @endif

</body>

</html>